package com.example.flickr_antonis.base

import dagger.android.support.AndroidSupportInjection
import android.app.Activity
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBar
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import com.hannesdorfmann.mosby3.mvp.MvpPresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import com.hannesdorfmann.mosby3.mvp.viewstate.MvpViewStateFragment
import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState
import javax.inject.Inject
import javax.inject.Provider
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

abstract class BaseMvpFragment<VIEW : MvpView, PRESENTER : MvpPresenter<VIEW>, VS : ViewState<VIEW>> : MvpViewStateFragment<VIEW, PRESENTER, VS>() {

    @Inject lateinit var presenterProvider: Provider<PRESENTER>

    override fun createPresenter(): PRESENTER {
        return presenterProvider.get()
    }

    override abstract fun createViewState(): VS

    protected fun setTitle(title: String) {
        actionBar?.title = title
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(layoutRes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeHomeBack()
    }

    protected abstract val layoutRes: Int

    protected abstract fun hasHomeBackEnabled(): Boolean

    protected fun initializeHomeBack() {
        actionBar?.setDisplayHomeAsUpEnabled(hasHomeBackEnabled())
    }

    protected val actionBar: ActionBar?
        get() = (activity as? AppCompatActivity)?.supportActionBar

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    fun <V: View> Fragment.bindView(id: Int) : ReadOnlyProperty<Fragment, V> {
        return FindViewByIdDelegate(id)
    }

    private class FindViewByIdDelegate <V: View> (
        private val id: Int
    ) : ReadOnlyProperty<Fragment, V> {

        override fun getValue(thisRef: Fragment, property: KProperty<*>): V {
            return thisRef.view?.findViewById<V>(id)
                ?: throw IllegalArgumentException("View is not bounded yet for fragment ${thisRef.tag}")
        }

    }
}