package com.example.flickr_antonis.base

interface Mapper<From, To> {

    fun fromConvert(to: To): From

    fun toConvert(from: From): To


    fun from(toItemList: List<To>): List<From> {
        return toItemList.map { fromConvert(it) }
    }

    fun to(fromItemList: List<From>): List<To> {
        return fromItemList.map { toConvert(it) }
    }
}