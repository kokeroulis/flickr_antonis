@file:Module
package com.example.flickr_antonis

import com.example.flickr_antonis.dagger.scopes.FragmentScope
import com.example.flickr_antonis.presentation.author.FlickAuthorFragment
import com.example.flickr_antonis.presentation.author.FlickAuthorModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = arrayOf(FlickAuthorModule::class))
    internal abstract fun contributesFlickAuthorFragmentInjector(): FlickAuthorFragment
}