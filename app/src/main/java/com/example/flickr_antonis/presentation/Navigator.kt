package com.example.flickr_antonis.presentation

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import com.example.flickr_antonis.presentation.author.FlickAuthorFragment
import javax.inject.Inject

class Navigator @Inject constructor() {

   // val supportFragmentManager: FragmentManager
    // kind of an overkill for just one fragment, but this is how we
    // should do it if we had more fragments. Also if we needed some parameter,
    // then we were going to pass it here
    fun openFlickAuthorFragment(activity: Activity, containerId: Int, tag: String) {
       val supportFragmentManager = (activity as? AppCompatActivity)?.supportFragmentManager
       supportFragmentManager?.let {
           supportFragmentManager.beginTransaction()
               .add(containerId, FlickAuthorFragment(), tag)
               .commit()
       }
    }
}
