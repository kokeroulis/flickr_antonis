package com.example.flickr_antonis.presentation.author

// This model is the same with the FlickAuthor
// despite this fact sometimes we might need to combine 2 domain models
// into to 1 of the presentation. So this is more of an example here
data class FlickAuthorModel(
    val authorId: String,
    val author: String,
    val imageUrl: String?
)
