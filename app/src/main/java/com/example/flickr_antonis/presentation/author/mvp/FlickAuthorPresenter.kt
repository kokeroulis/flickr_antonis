package com.example.flickr_antonis.presentation.author.mvp

import com.example.flickr_antonis.domain.GetFlickAuthorInteractor
import com.example.flickr_antonis.domain.Option
import com.example.flickr_antonis.presentation.author.FlickAuthorModelMapper
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

class FlickAuthorPresenter (
    private val flickAuthorInteractor: GetFlickAuthorInteractor,
    private val mapper: FlickAuthorModelMapper
) : MvpBasePresenter<FlickAuthorView>() {

    private var disposable: Disposable? = null

    fun loadAuthors() {
        flickAuthorInteractor.getResult(Option.None)
            .map(mapper::to)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { view?.showLoadingBar() }
            .subscribe(
                { authorList ->
                    view?.let {
                        view.hideLoadingBar()
                        view.loadAuthorList(authorList)
                    }
                },
                { error ->
                    view?.let {
                        view.hideLoadingBar()
                        view.showError(error)
                    }
                })
    }

    override fun detachView(retainInstance: Boolean) {
        super.detachView(retainInstance)
        if (!retainInstance && disposable?.isDisposed ?: false) {
            disposable?.dispose()
            disposable = null
        }
    }
}