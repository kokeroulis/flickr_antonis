package com.example.flickr_antonis.presentation.author

import com.example.flickr_antonis.base.Mapper
import com.example.flickr_antonis.domain.models.FlickAuthor
import javax.inject.Inject


class FlickAuthorModelMapper @Inject constructor() : Mapper<FlickAuthor, FlickAuthorModel> {

    override fun fromConvert(to: FlickAuthorModel): FlickAuthor {
        return FlickAuthor(to.authorId, to.author, to.imageUrl)
    }

    override fun toConvert(from: FlickAuthor): FlickAuthorModel {
        return FlickAuthorModel(from.authorId, from.author, from.imageUrl)
    }
}