package com.example.flickr_antonis.presentation.author.mvp

import com.example.flickr_antonis.presentation.author.FlickAuthorModel
import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState
import timber.log.Timber

class FlickAuthorViewState : ViewState<FlickAuthorView> {

    var authorList: List<FlickAuthorModel>? = null

    override fun apply(view: FlickAuthorView, retained: Boolean) {
        authorList?.let {
            Timber.d("The authors has been restored from the viewstate")
            view.hideLoadingBar()
            view.loadAuthorList(authorList!!)
        }
    }
}