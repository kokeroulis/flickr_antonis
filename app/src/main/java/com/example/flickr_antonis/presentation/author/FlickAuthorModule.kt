package com.example.flickr_antonis.presentation.author

import com.example.flickr_antonis.dagger.scopes.FragmentScope
import com.example.flickr_antonis.domain.GetFlickAuthorInteractor
import com.example.flickr_antonis.presentation.author.mvp.FlickAuthorPresenter
import dagger.Module
import dagger.Provides

@Module
class FlickAuthorModule {

    @Provides
    @FragmentScope
    fun providesFlickAuthorPresenter(flickAuthorInteractor: GetFlickAuthorInteractor,
                                     flickAuthorModelMapper: FlickAuthorModelMapper): FlickAuthorPresenter {
        return FlickAuthorPresenter(flickAuthorInteractor, flickAuthorModelMapper)
    }
}