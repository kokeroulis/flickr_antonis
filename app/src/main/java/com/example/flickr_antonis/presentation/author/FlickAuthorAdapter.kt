package com.example.flickr_antonis.presentation.author

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.flickr_antonis.R
import com.example.flickr_antonis.presentation.author.FlickAuthorAdapter.FlickAuthorViewHolder

class FlickAuthorAdapter : RecyclerView.Adapter<FlickAuthorViewHolder>() {

    var authorModelList: List<FlickAuthorModel>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlickAuthorViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.holder_flick_author, parent, false)
        return FlickAuthorViewHolder(view)
    }

    override fun onBindViewHolder(holder: FlickAuthorViewHolder, position: Int) {
        val model = authorModelList?.get(position)
        model?.let {
            holder.bind(model)
        }
    }

    override fun getItemCount(): Int {
        return authorModelList?.size ?: 0
    }


    class FlickAuthorViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val avatarView: ImageView = itemView.findViewById(R.id.avatarView)
        private val textView: TextView = itemView.findViewById(R.id.authorName)

        fun bind(model: FlickAuthorModel) {
            textView.text = model.author
            Glide.with(avatarView)
                .load(model.imageUrl)
                .into(avatarView)
        }
    }
}