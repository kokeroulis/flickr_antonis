package com.example.flickr_antonis.presentation.author

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import com.example.flickr_antonis.R
import com.example.flickr_antonis.base.BaseMvpFragment
import com.example.flickr_antonis.presentation.author.mvp.FlickAuthorPresenter
import com.example.flickr_antonis.presentation.author.mvp.FlickAuthorView
import com.example.flickr_antonis.presentation.author.mvp.FlickAuthorViewState
import timber.log.Timber

class FlickAuthorFragment : BaseMvpFragment<FlickAuthorView, FlickAuthorPresenter,
    FlickAuthorViewState>(), FlickAuthorView {

    val recyclerView: RecyclerView by bindView(R.id.rv)
    val progressBarView: ProgressBar by bindView(R.id.progressBar)

    private val adapter = FlickAuthorAdapter()

    override fun onNewViewStateInstance() {
        presenter.loadAuthors()
    }

    override fun createViewState(): FlickAuthorViewState {
        return FlickAuthorViewState()
    }

    override val layoutRes = R.layout.fragment_author

    override fun hasHomeBackEnabled() = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTitle("List of Authors")
        initRecyclerView()
    }

    override fun loadAuthorList(authorModelList: List<FlickAuthorModel>) {
        adapter.authorModelList = authorModelList
        adapter.notifyDataSetChanged()
        getViewState().authorList = authorModelList
    }

    override fun showError(error: Throwable) {
        Timber.e(error, "we had an error")
        Toast.makeText(context, error.message, Toast.LENGTH_SHORT).show()
    }

    override fun showLoadingBar() {
        progressBarView.visibility = View.VISIBLE
        recyclerView.visibility = View.GONE
    }

    override fun hideLoadingBar() {
        progressBarView.visibility = View.GONE
        recyclerView.visibility = View.VISIBLE
    }

    private fun initRecyclerView() {
        val lm = LinearLayoutManager(context)
        recyclerView.layoutManager = lm
        recyclerView.adapter = adapter
    }

    companion object {

        val TAG = FlickAuthorFragment::class.java.simpleName
    }
}