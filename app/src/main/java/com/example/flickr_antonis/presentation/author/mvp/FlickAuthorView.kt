package com.example.flickr_antonis.presentation.author.mvp

import com.example.flickr_antonis.presentation.author.FlickAuthorModel
import com.hannesdorfmann.mosby3.mvp.MvpView

interface FlickAuthorView : MvpView {
    fun loadAuthorList(authorModelList: List<FlickAuthorModel>)

    fun showError(error: Throwable)

    fun showLoadingBar()

    fun hideLoadingBar()
}