package com.example.flickr_antonis.dagger.modules

import com.example.flickr_antonis.MainActivity
import com.example.flickr_antonis.MainActivityModule
import com.example.flickr_antonis.dagger.scopes.ActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector



@Module
abstract class ActivityContributorModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = arrayOf(MainActivityModule::class))
    abstract fun contributesMainActivityInjector(): MainActivity
}