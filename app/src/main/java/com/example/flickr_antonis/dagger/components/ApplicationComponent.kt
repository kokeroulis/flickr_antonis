package com.example.flickr_antonis.dagger.components

import com.example.flickr_antonis.MyApplication
import com.example.flickr_antonis.data.network.ReplaceBaseUrl
import com.example.flickr_antonis.data.network.RestService
import com.example.flickr_antonis.dagger.modules.ActivityContributorModule
import com.example.flickr_antonis.dagger.modules.ApplicationModule
import com.example.flickr_antonis.dagger.modules.DatabaseModule
import com.example.flickr_antonis.dagger.modules.NetworkModule
import com.example.flickr_antonis.data.repository.FlickAuthorDataModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
    ApplicationModule::class,
    NetworkModule::class,
    ActivityContributorModule::class,
    DatabaseModule::class,
    FlickAuthorDataModule::class)
)
interface ApplicationComponent {

    fun inject(application: MyApplication)

    fun restService(): RestService
    fun replaceBaseUrl(): ReplaceBaseUrl
}