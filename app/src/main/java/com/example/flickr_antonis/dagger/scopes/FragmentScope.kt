package com.example.flickr_antonis.dagger.scopes

import java.lang.annotation.Documented
import javax.inject.Scope
import kotlin.annotation.AnnotationRetention.RUNTIME

@Scope
@MustBeDocumented
@Retention(RUNTIME)
annotation class FragmentScope
