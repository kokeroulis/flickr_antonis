package com.example.flickr_antonis.dagger.modules

import com.example.flickr_antonis.data.database.ApplicationDatabase
import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import javax.inject.Singleton
import dagger.Provides

@Module
class DatabaseModule {

    @Provides
    @Singleton
    internal fun providesApplicationDatabase(@Singleton context: Context): ApplicationDatabase {
        return Room.databaseBuilder(context, ApplicationDatabase::class.java, "application_db").build()
    }
}
