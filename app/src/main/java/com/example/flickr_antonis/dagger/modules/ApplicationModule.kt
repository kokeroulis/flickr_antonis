package com.example.flickr_antonis.dagger.modules

import android.app.Application
import android.content.Context
import com.example.flickr_antonis.data.network.ReplaceBaseUrl
import dagger.Module
import javax.inject.Singleton
import dagger.Provides


@Module
class ApplicationModule(private val application: Application) {

    @Provides
    @Singleton
    fun providesApplicationContext(): Context {
        return application.getApplicationContext()
    }

    @Provides
    @Singleton
    fun providesReplaceBaseUrl(): ReplaceBaseUrl {
        return ReplaceBaseUrl()
    }
}