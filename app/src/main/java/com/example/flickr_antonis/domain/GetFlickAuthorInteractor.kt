package com.example.flickr_antonis.domain

import com.example.flickr_antonis.domain.models.FlickAuthor
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class GetFlickAuthorInteractor @Inject constructor(
    private val flickAuthorRepository: FlickRepository
) : Interactor<Any, List<FlickAuthor>> {

    override fun getResult(param: Option<Any>): Flowable<List<FlickAuthor>> {
        return flickAuthorRepository.getAuthors()
            .flatMapSingle(this::fetchAuthorList)
            .filter { it.isNotEmpty() }
    }

    private fun fetchAuthorList(authorList: List<FlickAuthor>): Single<List<FlickAuthor>> {
        return fetchWhenAuthorIsEmpty(authorList).andThen(Single.just(authorList))
    }

    private fun fetchWhenAuthorIsEmpty(authorList: List<FlickAuthor>): Completable {
        return if (authorList.isEmpty()) flickAuthorRepository.forceUpdate() else Completable.complete()
    }
}