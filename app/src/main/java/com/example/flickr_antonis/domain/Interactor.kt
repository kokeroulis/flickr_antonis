package com.example.flickr_antonis.domain

import io.reactivex.Flowable

// We don't need the param in this example, but this is how we could
// approach the scenario in which we might need to pass something inside our Interactor.
interface Interactor<Param, Result> {

    fun getResult(param: Option<Param>): Flowable<Result>
}