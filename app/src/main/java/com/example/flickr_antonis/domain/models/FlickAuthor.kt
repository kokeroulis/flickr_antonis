package com.example.flickr_antonis.domain.models

data class FlickAuthor(
    val authorId: String,
    val author: String,
    val imageUrl: String?
)