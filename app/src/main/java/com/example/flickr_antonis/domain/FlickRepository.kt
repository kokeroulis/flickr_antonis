package com.example.flickr_antonis.domain

import com.example.flickr_antonis.domain.models.FlickAuthor
import io.reactivex.Completable
import io.reactivex.Flowable

interface FlickRepository {

    fun getAuthors(): Flowable<List<FlickAuthor>>

    fun forceUpdate(): Completable
}
