package com.example.flickr_antonis

import com.example.flickr_antonis.dagger.components.ApplicationComponent
import com.facebook.stetho.Stetho
import android.app.Activity
import android.app.Application
import com.example.flickr_antonis.dagger.components.DaggerApplicationComponent
import com.example.flickr_antonis.dagger.modules.ApplicationModule
import com.squareup.leakcanary.LeakCanary
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import javax.inject.Inject
import dagger.android.HasActivityInjector
import timber.log.Timber
import timber.log.Timber.DebugTree


open class MyApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchingActivityInjector
    }

    override fun onCreate() {
        super.onCreate()
        applicationComponent = createComponent()
        applicationComponent.inject(this)

        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
            Stetho.initializeWithDefaults(this)
        }

        // Robolectric doesn't work property with LeakCanary
        if (!isUnderTest()) {
            LeakCanary.install(this)
        }
    }

    protected open fun isUnderTest(): Boolean {
        return false
    }

    private fun createComponent(): ApplicationComponent {
        return DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }

    companion object {
        // required for unit test
        lateinit var applicationComponent: ApplicationComponent
    }
}
