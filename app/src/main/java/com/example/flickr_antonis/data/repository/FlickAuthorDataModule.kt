package com.example.flickr_antonis.data.repository

import com.example.flickr_antonis.data.database.ApplicationDatabase
import com.example.flickr_antonis.data.entity.mappers.FlickAuthorEntityMapper
import com.example.flickr_antonis.data.entity.mappers.FlickAuthorMapper
import com.example.flickr_antonis.data.network.RestService
import com.example.flickr_antonis.domain.FlickRepository
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class FlickAuthorDataModule {

    @Provides
    @Singleton
    @Named(LOCAL_STORE)
    fun  providesLocalFlickAuthorDataStore(applicationDatabase: ApplicationDatabase): FlickAuthorDataStore {
        return LocalFlickAuthorDataStore(applicationDatabase.flickAuthorDao())
    }

    @Provides
    @Singleton
    @Named(REMOTE_STORE)
    fun  providesRemoteFlickAuthorDataStore(restService: RestService,
                                            mapper: FlickAuthorEntityMapper): FlickAuthorDataStore {
        return RemoteFlickAuthorDataStore(restService, mapper)
    }

    @Provides
    @Singleton
    fun providesFlickAuthorRepository(@Named(LOCAL_STORE) localDataStore: FlickAuthorDataStore,
                               @Named(REMOTE_STORE) remoteDataStore: FlickAuthorDataStore,
                               mapper: FlickAuthorMapper): FlickRepository {
        return FlickAuthorDataRepository(localDataStore, remoteDataStore, mapper)
    }

    companion object {
        const val LOCAL_STORE = "local_store"
        const val REMOTE_STORE = "remote_store"
    }
}