package com.example.flickr_antonis.data.repository

import com.example.flickr_antonis.data.database.FlickAuthorDao
import com.example.flickr_antonis.data.entity.FlickAuthorEntity
import io.reactivex.Completable
import io.reactivex.Flowable

class LocalFlickAuthorDataStore(private val authorDao: FlickAuthorDao) : FlickAuthorDataStore {

    override fun getAuthors(): Flowable<List<FlickAuthorEntity>> {
        return authorDao.getAuthors()
    }

    override fun saveAuthors(authorEntityList: List<FlickAuthorEntity>): Completable {
        return Completable.create { emitter ->
            authorDao.insertAll(authorEntityList)
            emitter.onComplete()
        }
    }
}