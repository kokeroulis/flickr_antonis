package com.example.flickr_antonis.data.repository

import com.example.flickr_antonis.data.entity.FlickAuthorEntity
import io.reactivex.Completable
import io.reactivex.Flowable

interface FlickAuthorDataStore {

    fun getAuthors(): Flowable<List<FlickAuthorEntity>>

    fun saveAuthors(authorEntityList: List<FlickAuthorEntity>): Completable
}