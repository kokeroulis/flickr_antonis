package com.example.flickr_antonis.data.repository

import com.example.flickr_antonis.base.Mapper
import com.example.flickr_antonis.data.entity.FlickAuthorEntity
import com.example.flickr_antonis.data.network.FlickAuthorResponse
import com.example.flickr_antonis.data.network.RestService
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers

class RemoteFlickAuthorDataStore(
    private val restService: RestService,
    private val mapper: Mapper<FlickAuthorResponse, FlickAuthorEntity>
) : FlickAuthorDataStore {

    override fun getAuthors(): Flowable<List<FlickAuthorEntity>> {
        return restService.postList
            .subscribeOn(Schedulers.io())
            .map { it.items }
            .map(mapper::to)
            .toFlowable()
    }

    override fun saveAuthors(authorEntityList: List<FlickAuthorEntity>): Completable {
        throw IllegalArgumentException("saveAuthors operation is not supported")
    }
}