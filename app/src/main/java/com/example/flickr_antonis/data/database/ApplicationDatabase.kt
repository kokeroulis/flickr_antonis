package com.example.flickr_antonis.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.flickr_antonis.data.entity.FlickAuthorEntity

@Database(entities = arrayOf(FlickAuthorEntity::class), version = 1, exportSchema = false)
abstract class ApplicationDatabase : RoomDatabase() {

    abstract fun flickAuthorDao(): FlickAuthorDao
}