package com.example.flickr_antonis.data.network

data class FlickFeedResponse(val items: List<FlickAuthorResponse>)