package com.example.flickr_antonis.data.network

import io.reactivex.Single
import retrofit2.http.GET

interface RestService {

    @get:GET("photos_public.gne?format=json&nojsoncallback=1")
    val postList: Single<FlickFeedResponse>
}