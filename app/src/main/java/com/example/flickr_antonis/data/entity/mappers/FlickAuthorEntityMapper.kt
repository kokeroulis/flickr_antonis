package com.example.flickr_antonis.data.entity.mappers

import com.example.flickr_antonis.base.Mapper
import com.example.flickr_antonis.data.entity.FlickAuthorEntity
import com.example.flickr_antonis.data.network.FlickAuthorResponse
import javax.inject.Inject

class FlickAuthorEntityMapper @Inject constructor() : Mapper<FlickAuthorResponse, FlickAuthorEntity> {

    override fun fromConvert(to: FlickAuthorEntity): FlickAuthorResponse {
        throw IllegalArgumentException("convert to FlickAuthorResponse is not supported")
    }

    override fun toConvert(from: FlickAuthorResponse): FlickAuthorEntity {
        return FlickAuthorEntity(from.author_id, from.author, from.imageUrl)
    }

}