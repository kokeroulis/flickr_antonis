package com.example.flickr_antonis.data.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.example.flickr_antonis.data.entity.FlickAuthorEntity
import io.reactivex.Flowable

@Dao
interface FlickAuthorDao {

    @Query("Select * from author")
    fun getAuthors(): Flowable<List<FlickAuthorEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(userList: List<FlickAuthorEntity>)
}