package com.example.flickr_antonis.data.repository

import com.example.flickr_antonis.base.Mapper
import com.example.flickr_antonis.data.entity.FlickAuthorEntity
import com.example.flickr_antonis.domain.FlickRepository
import com.example.flickr_antonis.domain.models.FlickAuthor
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

@Singleton
class FlickAuthorDataRepository(
    private val localDataStore: FlickAuthorDataStore,
    private val remoteDataStore: FlickAuthorDataStore,
    private val mapper: Mapper<FlickAuthorEntity, FlickAuthor>) : FlickRepository {

    override fun getAuthors(): Flowable<List<FlickAuthor>> {
        return localDataStore.getAuthors()
            .subscribeOn(Schedulers.io())
            .map(mapper::to)
    }

    override fun forceUpdate(): Completable {
        return remoteDataStore.getAuthors()
            .subscribeOn(Schedulers.io())
            .firstOrError()
            .flatMapCompletable(localDataStore::saveAuthors)
    }
}