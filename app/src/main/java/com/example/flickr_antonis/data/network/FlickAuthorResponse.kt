package com.example.flickr_antonis.data.network

data class FlickAuthorResponse(val author: String,
                               val author_id: String,
                               val media: Map<String, Any>) {

    val imageUrl: String? get() = media["m"] as? String
}