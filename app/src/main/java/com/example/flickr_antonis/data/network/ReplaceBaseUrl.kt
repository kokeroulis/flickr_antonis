package com.example.flickr_antonis.data.network

import java.util.concurrent.atomic.AtomicReference
import javax.inject.Inject


class ReplaceBaseUrl  {

    @Inject
    constructor() : this(PRODUCTION_API_URL)

    constructor(defaultUrl: String) {
        baseUrl = AtomicReference(defaultUrl)
    }

    val baseUrl: AtomicReference<String>

    fun changeUrl(url: String) {
        baseUrl.set(url)
    }

    companion object {
        const val PRODUCTION_API_URL: String = "https://api.flickr.com/services/feeds/"
    }
}