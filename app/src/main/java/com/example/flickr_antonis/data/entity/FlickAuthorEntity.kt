package com.example.flickr_antonis.data.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "author")
data class FlickAuthorEntity(
    @PrimaryKey
    val authorId: String,
    val author: String,
    val imageUrl: String?
)