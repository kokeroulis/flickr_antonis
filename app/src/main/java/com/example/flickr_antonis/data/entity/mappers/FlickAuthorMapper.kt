package com.example.flickr_antonis.data.entity.mappers

import com.example.flickr_antonis.base.Mapper
import com.example.flickr_antonis.data.entity.FlickAuthorEntity
import com.example.flickr_antonis.domain.models.FlickAuthor
import javax.inject.Inject

class FlickAuthorMapper @Inject constructor() : Mapper<FlickAuthorEntity, FlickAuthor> {

    override fun fromConvert(to: FlickAuthor): FlickAuthorEntity {
        return FlickAuthorEntity(to.authorId, to.author, to.imageUrl)
    }

    override fun toConvert(from: FlickAuthorEntity): FlickAuthor {
        return FlickAuthor(from.authorId, from.author, from.imageUrl)
    }

}