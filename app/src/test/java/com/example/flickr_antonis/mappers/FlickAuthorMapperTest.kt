package com.example.flickr_antonis.mappers

import com.example.flickr_antonis.data.entity.mappers.FlickAuthorEntityMapper
import com.example.flickr_antonis.data.entity.mappers.FlickAuthorMapper
import com.example.flickr_antonis.dataProvider.DummyFlickAuthorResponse
import com.example.flickr_antonis.dataProvider.DummyFlickFeedResponse
import org.assertj.core.api.Java6Assertions.assertThat
import org.junit.Test

class FlickAuthorMapperTest {

    val mapperEntiry = FlickAuthorEntityMapper()
    val mapper = FlickAuthorMapper()

    @Test
    fun testDomainMapperOk() {
        val authorListResponse = DummyFlickFeedResponse.create().items
        val authorListEntity = mapperEntiry.to(authorListResponse)

        val author = mapper.to(authorListEntity).first()

        assertThat(author.author).isEqualTo("author_a")
        assertThat(author.authorId).isEqualTo("author_1")
        assertThat(author.imageUrl).isEqualTo(DummyFlickAuthorResponse.DUMMY_AVATAR)
    }
}