package com.example.flickr_antonis.mappers

import com.example.flickr_antonis.data.entity.mappers.FlickAuthorEntityMapper
import com.example.flickr_antonis.dataProvider.DummyFlickAuthorResponse
import com.example.flickr_antonis.dataProvider.DummyFlickFeedResponse
import org.assertj.core.api.Java6Assertions.assertThat
import org.junit.Test

class FlickAuthorEntityMapperTest {

    val mapper = FlickAuthorEntityMapper()

    @Test
    fun testEntityMapperOk() {
        val authorList = DummyFlickFeedResponse.create().items

        val authorEntity = mapper.to(authorList).first()
        assertThat(authorEntity.author).isEqualTo("author_a")
        assertThat(authorEntity.authorId).isEqualTo("author_1")
        assertThat(authorEntity.imageUrl).isEqualTo(DummyFlickAuthorResponse.DUMMY_AVATAR)
    }
}