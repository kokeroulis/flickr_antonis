package com.example.flickr_antonis.mappers

import com.example.flickr_antonis.data.entity.mappers.FlickAuthorEntityMapper
import com.example.flickr_antonis.data.entity.mappers.FlickAuthorMapper
import com.example.flickr_antonis.dataProvider.DummyFlickAuthorResponse
import com.example.flickr_antonis.dataProvider.DummyFlickFeedResponse
import com.example.flickr_antonis.presentation.author.FlickAuthorModelMapper
import org.assertj.core.api.Java6Assertions.assertThat
import org.junit.Test

class FlickAuthorModelMapperTest {

    val mapperEntiry = FlickAuthorEntityMapper()
    val mapperDomain = FlickAuthorMapper()
    val mapper = FlickAuthorModelMapper()

    @Test
    fun testPresentationModelMapperOk() {
        val authorListResponse = DummyFlickFeedResponse.create().items

        val authorListEntity = mapperEntiry.to(authorListResponse)

        val authorDomain = mapperDomain.to(authorListEntity)
        val author = mapper.to(authorDomain).first()

        assertThat(author.author).isEqualTo("author_a")
        assertThat(author.authorId).isEqualTo("author_1")
        assertThat(author.imageUrl).isEqualTo(DummyFlickAuthorResponse.DUMMY_AVATAR)
    }
}