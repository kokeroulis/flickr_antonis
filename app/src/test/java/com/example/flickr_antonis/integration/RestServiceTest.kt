package com.example.flickr_antonis.integration

import com.example.flickr_antonis.BuildConfig
import com.example.flickr_antonis.MyApplication
import com.example.flickr_antonis.base.MockServerSetupHelper
import com.example.flickr_antonis.base.UnitTestApplication
import com.example.flickr_antonis.data.network.RestService
import okhttp3.mockwebserver.MockWebServer
import org.assertj.core.api.Java6Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(constants = BuildConfig::class, application = UnitTestApplication::class)
class RestServiceTest {

    private lateinit var restService: RestService
    private lateinit var mockWebServer: MockWebServer

    @Before
    fun before() {
        mockWebServer = MockServerSetupHelper.createAndSetupMockWebServer()
        restService = MyApplication.applicationComponent.restService()
    }

    @Test
    fun testFlickFeedReponse() {
        MockServerSetupHelper.enqueueJsonAssetToMockWebServer("flick_response.json", mockWebServer)

        val response = restService.postList.toObservable().blockingFirst()
        val author = response.items.first()
        assertThat(author.author).isEqualTo("nobody@flickr.com (\"linstyle001\")")
        assertThat(author.author_id).isEqualTo("153155851@N05")
        val avatarUrl = author.imageUrl
        assertThat(avatarUrl).isEqualTo("https://farm5.staticflickr.com/4464/23972947028_fe2a6654a3_m.jpg")
    }
}