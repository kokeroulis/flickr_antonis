package com.example.flickr_antonis.dataProvider

import com.example.flickr_antonis.data.network.FlickAuthorResponse

object DummyFlickAuthorResponse {

    const val DUMMY_AVATAR = "https://dummy_avatar.com/avatar.jpg"

    fun create(): List<FlickAuthorResponse> {
        val map = mapOf("m" to DUMMY_AVATAR)
        return listOf(
            FlickAuthorResponse("author_a", "author_1", map),
            FlickAuthorResponse("author_b", "author_2", map),
            FlickAuthorResponse("author_c", "author_3", map)
        )
    }
}