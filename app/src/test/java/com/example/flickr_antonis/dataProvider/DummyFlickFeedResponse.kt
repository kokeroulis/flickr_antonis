package com.example.flickr_antonis.dataProvider

import com.example.flickr_antonis.data.network.FlickFeedResponse

object DummyFlickFeedResponse {

    fun create(): FlickFeedResponse {
        return FlickFeedResponse(DummyFlickAuthorResponse.create())
    }
}