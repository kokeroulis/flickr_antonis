package com.example.flickr_antonis.base

import com.example.flickr_antonis.MyApplication

class UnitTestApplication : MyApplication() {

    override fun isUnderTest() = true
}