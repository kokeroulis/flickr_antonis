package com.example.flickr_antonis.base

import com.example.flickr_antonis.MyApplication
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader

object MockServerSetupHelper {

    @Throws(IOException::class)
    fun createAndSetupMockWebServer(): MockWebServer {
        val mockWebServer = MockWebServer()
        mockWebServer.start()

        val baseUrl = MyApplication.applicationComponent.replaceBaseUrl()
        baseUrl.changeUrl(mockWebServer.url("").toString())
        return mockWebServer
    }

    @Throws(IOException::class)
    fun enqueueJsonAssetToMockWebServer(assetName: String,
                                        mockWebServer: MockWebServer) {
        val json = readStream(assetToStream(assetName))
        mockWebServer.enqueue(MockResponse().setBody(json))
    }

    @Throws(IOException::class)
    private fun assetToStream(assetName: String): InputStream {
        return javaClass.getResourceAsStream(assetName)
    }

    @Throws(IOException::class)
    private fun readStream(iStream: InputStream): String {

        //Buffered reader allows us to read line by line
        BufferedReader(InputStreamReader(iStream)).use({ bReader ->
            val builder = StringBuilder()
            var line: String? = bReader.readLine()
            while (line != null) {  //Read till end
                builder.append(line)
                builder.append("\n") // append new line to preserve lines
                line = bReader.readLine()
            }
            return builder.toString()
        })
    }
}