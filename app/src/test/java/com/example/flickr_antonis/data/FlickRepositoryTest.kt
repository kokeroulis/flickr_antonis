package com.example.flickr_antonis.data

import com.example.flickr_antonis.base.BaseRxTest
import com.example.flickr_antonis.data.entity.FlickAuthorEntity
import com.example.flickr_antonis.data.entity.mappers.FlickAuthorEntityMapper
import com.example.flickr_antonis.data.entity.mappers.FlickAuthorMapper
import com.example.flickr_antonis.data.repository.FlickAuthorDataRepository
import com.example.flickr_antonis.data.repository.FlickAuthorDataStore
import com.example.flickr_antonis.dataProvider.DummyFlickAuthorResponse
import com.example.flickr_antonis.domain.FlickRepository
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.processors.BehaviorProcessor
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import java.util.*

class FlickRepositoryTest : BaseRxTest() {

    private val entityList: List<FlickAuthorEntity> by lazy {
        val response = DummyFlickAuthorResponse.create()
        val mapper = FlickAuthorEntityMapper()
        Collections.unmodifiableList(mapper.to(response))
    }

    private lateinit var repository: FlickRepository
    private lateinit var localDataStore: FlickAuthorDataStore
    private lateinit var remoteDataStore: FlickAuthorDataStore

    private lateinit var mapper: FlickAuthorMapper

    @Before
    fun before() {
        localDataStore = mock()
        remoteDataStore = mock()
        mapper = FlickAuthorMapper()
        repository = FlickAuthorDataRepository(localDataStore, remoteDataStore, FlickAuthorMapper())
    }

    @Test
    fun emitItemsFromRepository() {
        StreamBuilder().emitItems(localDataStore, remoteDataStore, entityList)
        val subscriber = repository.getAuthors().test()
        subscriber.assertNoErrors()
        subscriber.assertNotComplete()
        subscriber.assertValue(mapper.to(entityList))
    }

    @Test
    fun getItemsFromCloudAndSaveToDb() {
        StreamBuilder().emitItemsFromCloud(localDataStore, remoteDataStore, entityList)
        val subscriber = repository.forceUpdate().test()
        subscriber.assertNoErrors()
        subscriber.assertComplete()
    }

    private class StreamBuilder {
        private val subject: BehaviorProcessor<List<FlickAuthorEntity>> = BehaviorProcessor.create()

        fun emitItems(localDataStore: FlickAuthorDataStore, remoteDataStore: FlickAuthorDataStore,
                      items: List<FlickAuthorEntity>) {
            whenever(localDataStore.getAuthors()).thenReturn(subject)
            verify(remoteDataStore, never()).getAuthors()
            subject.onNext(items)
        }

        fun emitItemsFromCloud(localDataStore: FlickAuthorDataStore, remoteDataStore: FlickAuthorDataStore,
                               items: List<FlickAuthorEntity>) {
            whenever(remoteDataStore.getAuthors()).thenReturn(Single.just(items).toFlowable())
            whenever(localDataStore.saveAuthors(Mockito.anyList())).thenReturn(Completable.complete())
        }
    }
}
