package com.example.flickr_antonis.presentation

import com.example.flickr_antonis.base.BaseRxTest
import com.example.flickr_antonis.data.entity.mappers.FlickAuthorEntityMapper
import com.example.flickr_antonis.data.entity.mappers.FlickAuthorMapper
import com.example.flickr_antonis.dataProvider.DummyFlickAuthorResponse
import com.example.flickr_antonis.domain.GetFlickAuthorInteractor
import com.example.flickr_antonis.domain.Option
import com.example.flickr_antonis.domain.models.FlickAuthor
import com.example.flickr_antonis.presentation.author.FlickAuthorModelMapper
import com.example.flickr_antonis.presentation.author.mvp.FlickAuthorPresenter
import com.example.flickr_antonis.presentation.author.mvp.FlickAuthorView
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.robolectric.RobolectricTestRunner
import java.util.*

// We could use the Robolectric on the rest of our tests if it was needed.
// In that case we would have annotated the BaseRxTest class
@RunWith(RobolectricTestRunner::class)
class FlickAuthorPresenterTest : BaseRxTest() {

    private lateinit var interactor: GetFlickAuthorInteractor
    private lateinit var authorView: FlickAuthorView
    private lateinit var presenter: FlickAuthorPresenter

    private val authorList: List<FlickAuthor> by lazy {
        val response = DummyFlickAuthorResponse.create()
        val reponseMapper = FlickAuthorEntityMapper()
        val entityList = reponseMapper.to(response)
        val mapper = FlickAuthorMapper()

        Collections.unmodifiableList(mapper.to(entityList))
    }

    @Before
    fun before() {
        interactor = mock()
        presenter = FlickAuthorPresenter(interactor, FlickAuthorModelMapper())
        authorView = mock()
        presenter.attachView(authorView)
    }

    @Test
    fun loadAuthorsSuccessfully() {
        StreamBuilder().emitAuthorsSuccessfully(interactor, authorList)
        presenter.loadAuthors()

        val error = mock<Throwable>()
        verify(authorView).showLoadingBar()
        verify(authorView).loadAuthorList(Mockito.anyList())
        verify(authorView).hideLoadingBar()
        verify(authorView, never()).showError(error)
    }

    @Test
    fun whenLoadAuthorErrorOccur() {
        val error = mock<Throwable>()
        StreamBuilder().emitErrorWhenLoadAuthors(interactor, error)
        presenter.loadAuthors()

        verify(authorView).showLoadingBar()
        verify(authorView, never()).loadAuthorList(Mockito.anyList())
        verify(authorView).hideLoadingBar()
        verify(authorView).showError(error)
    }

    private class StreamBuilder {
        private val subject: BehaviorProcessor<List<FlickAuthor>> = BehaviorProcessor.create()

        fun emitAuthorsSuccessfully(interactor: GetFlickAuthorInteractor, authorList: List<FlickAuthor>) {
            whenever(interactor.getResult(Option.None)).thenReturn(subject)
            subject.onNext(authorList)
        }

        fun emitErrorWhenLoadAuthors(interactor: GetFlickAuthorInteractor, error: Throwable) {
            whenever(interactor.getResult(Option.None)).thenReturn(Flowable.error(error))
        }
    }
}