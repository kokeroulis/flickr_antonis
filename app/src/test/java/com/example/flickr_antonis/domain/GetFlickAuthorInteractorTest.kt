package com.example.flickr_antonis.domain

import com.example.flickr_antonis.base.BaseRxTest
import com.example.flickr_antonis.data.entity.mappers.FlickAuthorEntityMapper
import com.example.flickr_antonis.data.entity.mappers.FlickAuthorMapper
import com.example.flickr_antonis.dataProvider.DummyFlickAuthorResponse
import com.example.flickr_antonis.domain.models.FlickAuthor
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Completable
import io.reactivex.processors.BehaviorProcessor
import org.junit.Before
import org.junit.Test
import java.io.IOException
import java.util.*

class GetFlickAuthorInteractorTest : BaseRxTest() {

    private val authorList: List<FlickAuthor> by lazy {
        val response = DummyFlickAuthorResponse.create()
        val reponseMapper = FlickAuthorEntityMapper()
        val entityList = reponseMapper.to(response)

        Collections.unmodifiableList(mapper.to(entityList))
    }

    private lateinit var interactor: GetFlickAuthorInteractor
    private lateinit var repository: FlickRepository
    private lateinit var mapper: FlickAuthorMapper


    @Before
    fun before() {
        repository = mock()
        mapper = FlickAuthorMapper()
        interactor = GetFlickAuthorInteractor(repository)
    }

    @Test
    fun getItemsFromCache() {
        StreamBuilder().emitItemsFromCache(repository, authorList)
        val subscriber = interactor.getResult(Option.None).test()
        subscriber.assertNoErrors()
        subscriber.assertNotComplete()
        subscriber.assertValue(authorList)
    }

    @Test
    fun fetchAuthorListFromCloudAndShow() {
        StreamBuilder().emitItemsFromCloudAndThenFromDb(repository, authorList)
        val subscriber = interactor.getResult(Option.None).test()

        subscriber.assertValueCount(1)
        subscriber.assertNoErrors()
        subscriber.assertNotComplete()
    }

    @Test
    fun fetchAuthorListAndFailOnNetwork() {
        val error = IOException("Bad internet connection")
        StreamBuilder().emitItemsFromCloudButFailOnNetwork(repository, error)
        val subscriber = interactor.getResult(Option.None).test()

        subscriber.assertError(error)
        subscriber.assertTerminated()
    }

    private class StreamBuilder {
        private val subject: BehaviorProcessor<List<FlickAuthor>> = BehaviorProcessor.create()

        fun emitItemsFromCache(repository: FlickRepository,
                               items: List<FlickAuthor>) {

            whenever(repository.getAuthors()).thenReturn(subject)
            verify(repository, never()).forceUpdate()
            subject.onNext(items)
        }

        fun emitItemsFromCloudAndThenFromDb(repository: FlickRepository, authorList: List<FlickAuthor>) {
            // emit an empty list in order to show that the data are absent
            subject.onNext(listOf())
            whenever(repository.getAuthors()).thenReturn(subject)

            doAnswer {
                subject.onNext(authorList)
                Completable.complete()
            }.whenever(repository).forceUpdate()
        }

        fun emitItemsFromCloudButFailOnNetwork(repository: FlickRepository, error: Throwable) {
            // emit an empty list in order to show that the data are absent
            subject.onNext(listOf())
            whenever(repository.getAuthors()).thenReturn(subject)
            whenever(repository.forceUpdate()).thenReturn(Completable.error(error))
        }
    }

}
