# General Structure

This project is being implemented using kotlin and the Android Gradle Plugin 3.0-beta7.

The project also contains unit tests for the integration with the remote resource,
mappers, repository, interactor and for the presenter. They can be executed 
from the command line (e.x. ./gradlew test)

Most of the test are not using Robolectric because it is not needed,
but if we had more tests which require Robolectric, then we would 
annotate the "BaseRxTest" class in order to use the same configuration everywhere.

# Libraries and architectures that are being used

Bellow you will find a list of the most important libraries and architectures
that are being used by this project.

Clean Architecture
Kotlin
RxJava2
Dagger 2 (2.11)
Room database
Glide
Retrofit2 (Http)
MVP (with Mosby 3)
Mockito
Robolectric 

# Details about the implementation

In the following project i am using a separate model for each 
layer(data, domain, presentation) and also for each service.

The implemented repository contains a method called "forceUpdate" which
can be called from the interactor in order to update the data. I followed
this approach in order to be easier to integrate refresh mechanisms like 
pull to refresh.

The presenter can survive the configuration changes because Mosby v3
can handle this for us. If we had to use another MVP library then we
could store our presenters inside a headless fragment in order to survive
configuration changes.